function requests(url) {
  return new Promise(function (resolve, reject) {
    let request = new XMLHttpRequest();
    request.open("GET", url);
    request.onload = () => {
      let data = JSON.parse(request.responseText);
      resolve(data);
    };
    request.send();
  });
}
let ol = document.createElement("ol");
let loader = document.getElementById('loaded')
document.body.appendChild(ol);
let requestFilms = requests("https://ajax.test-danit.com/api/swapi/films");
let requestPeople = requests("https://ajax.test-danit.com/api/swapi/people");
function getCharacterName(characterUrl) {
  return requests(characterUrl).then((characterData) => characterData.name);
}
Promise.all([requestFilms, requestPeople])
  .then(([filmsData, peopleData]) => {
    filmsData.forEach((film) => {
      const li = document.createElement("li");
      const charactersPromises = film.characters.map((characterUrl) =>
        getCharacterName(characterUrl)
      );
      Promise.all(charactersPromises).then((charactersNames) => {
        li.innerHTML = ` 
         <p>Episode: ${film.episodeId}</p>
         <p>Name: ${film.name}</p>
         <p>Title: ${film.openingCrawl}</p>
         <p>Characters: ${charactersNames.join(", ")}</p>  
        `;
        ol.appendChild(li);
        loader.remove()
      });
    });
  })
  .catch((error) => {
    console.error("Виникла помилка:", error);
  });
